#!/usr/bin/env python
# encoding: utf-8
"""
blackjack.py

Created by Sven Agnew on 2012-03-20.
Copyright (c) 2012 Studio2cb. All rights reserved.
"""

import sys
import os
from random import choice as choose

def main():
	pass

if __name__ == '__main__':
	main()
	print("Hello mo'fo'...")
	print("Welcome to the game of Blackjack")

deck = [2,3,4,5,6,7,8,9,10,10,10,10,11] # J,Q,K are all 10 in value and we declare Aces High.

"""
Set up some stuff for players 
"""
player_hand = []
player_busted = False
player_hand.append(choose(deck))
player_hand.append(choose(deck))

"""
Set up similar stuff for the computer
"""
computer_hand = []
computer_busted = False
computer_hand.append(choose(deck))
computer_hand.append(choose(deck))

"""
Grant a hand to the player and test for bust and (unlikely) blackjack; 
If neither offer hit or stick and continue thus
"""
while True:
    player_total = sum(player_hand)
    print "Your hand has cards %s whose value totals %d" % (player_hand, player_total)
    if player_total > 21:
        print "You are BUST! sucker..."
        player_busted = True
        break
    elif player_total == 21:
        print "You have BLACKJACK! Hooray!"
        break
    else:
        hit_or_stick = raw_input("Hit or Stand/Done (h or s): ").lower()
        if 'h' in hit_or_stick:
            player_hand.append(choose(deck))
        else:
            break

"""
Barely legal - as before but the computer keeps playing until it's over eighteen or bust.
;)
"""
while True:
    computer_total = sum(computer_hand)
    if computer_total < 18:
        computer_hand.append(choose(deck))
    else:
        break
print "The Dealer has cards %s whose value totals %d" % (computer_hand, computer_total)

"""
Rank the hands and dish out the bad news.
Gambling is for suckers... 
"""
if computer_total > 21:
    print "The computer is busted!"
    computer_busted = True
    if player_busted == False:
        print "You win!"
elif computer_total > player_total:
    print "The Dealer wins!"
elif computer_total == player_total:
    print "It's a draw!"
elif computer_total < player_total:
    if player_busted == False:
        print "You WIN!"
    elif computer_busted == False:
        print "The Dealer Wins"


"""
# TODO      Figure out how to keep scores across games.. a file ?
            Or simply keep playing until the players types "quit"
"""

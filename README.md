### Blackjack

`blackjack.py` is a simple command line version of the game
[Blackjack](http://en.wikipedia.org/wiki/Blackjack) (also sometimes known
as "21") implemented as a Python script.

The aim of the game is simple - to amass a highest combined card value
less than or equal to 21

Once downloaded, you may play the game by invoking `:~ $ python
blackjack.py` in your terminal.  
